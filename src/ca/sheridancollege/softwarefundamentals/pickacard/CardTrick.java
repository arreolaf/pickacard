/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
import java.util.Scanner;
import java.util.Random;

public class CardTrick {
    
    public static void main(String[] args)
    {
        Card[] magicHand = new Card[7];
        Random rand = new Random();
        Random rand2 = new Random();
        Card c = new Card();
        for (int i=0; i<magicHand.length; i++)
        {
            int rndm = rand.nextInt(13) + 1 ;
            int rndm2 = rand.nextInt(3);
            
            //c.setValue(insert call to random number generator here)
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
            c.setValue(rndm);
            c.setSuit(Card.SUITS[rndm2]);
            System.out.println(c.getSuit());
             System.out.println(c.getValue());
           
        }
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
        Card myCard = new Card ();
       
        
        Scanner in = new Scanner (System.in);
        System.out.println("Enter a card value: ");
        int myValue =  in.nextInt();
        myCard.setValue(myValue);
        
        System.out.println("Enter a card suit: ");
        String mySuit = in.next();
        myCard.setSuit(mySuit);
        
       for (int i = 0; i < magicHand.length; i++){
         if (myCard.getValue() == c.getValue() && myCard.getSuit().equals(c.getSuit())){
            System.out.println("Your card is in the magic hand ");
        }else{
            System.out.println("Your card is not in the magic hand");
         }
    }
}}
